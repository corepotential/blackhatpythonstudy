import socket
import requests
import ssl

t_host = "vulnerable"
t_port = 443


c = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
c.connect((t_host,t_port))
c.send(b"GET / HTTP/1.1\r\nHost: vulnerable\r\n\r\n")
getRequest = "GET /HTTP/1.1\r\nHost: vulnerable\r\n\r\n"
requests.packages.urllib3.disable_warnings()
r = requests.get('https://vulnerable/',verify=False)

context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
context.verify_mode = ssl.CERT_REQUIRED
context.check_hostname = True
context.load_verify_locations(cafile="/etc/ssl/certs/apache-selfsigned.crt")

ssl_socket = context.wrap_socket(c, server_side=False, do_handshake_on_connect=True, suppress_ragged_eofs=True, server_hostname='vulnerable')
ssl_socket.connect(('vulnerable', 443 ))
re = c.recv(4096)
ssl_socket.send(getRequest)
print ssl_socket.recv(1024)
print ssl_socket.recv(1024)
print (re.decode('UTF-8'))
print r.text
