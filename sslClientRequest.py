import socket
import requests

t_host = "127.0.0.1"
t_port = 80

c = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
c.connect((t_host,t_port))
c.send(b"GET / HTTP/1.1\r\nHost: vulnerable\r\n\r\n")
requests.packages.urllib3.disable_warnings()
r = requests.get('https://vulnerable/',verify=False)

re = c.recv(4096)
print (re.decode('UTF-8')), r.text
