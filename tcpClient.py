#imports the modules needed
import socket

#sets what the target will be
target_host = "www.google.com"
target_port = 80

#creates a socket object(AF=standard IPv4 or hostname, sock_stream implies it being a TCP client)
client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

#connects the client to the server
client.connect((target_host,target_port))

#sends it some data
client.send(b"GET / HTTP/1.1\r\nHost: google.com\r\n\r\n")

#recieves data back and prints out the response
response = client.recv(4096)

print (response.decode('UTF-8'))
