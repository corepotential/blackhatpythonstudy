#imports modules
import socket

#determines the targets you want
target_host = "127.0.0.1"
target_port = 80


#create a socket object)
def upd_client():
     client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
     data = b'AAABBBCCC'
     client.bind ((target_host, target_port))
#send some data
     client.sendto(data, (target_host, target_port))
#recieve data
     data, addr = client.recvfrom(4096)
     print (data.decode('UTF-8'), addr)

if __name__ == '__main__':
    upd_client()
